# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $('#calendar').fullCalendar
    header:
      left: 'today prev,next'
      center: 'title'
      right: 'month,agendaWeek,agendaDay'
    firstDay: 1
    fixedWeekCount: false
    height: "auto"
    # タイトルの書式
    titleFormat:
      month: 'YYYY年MMM月'
      agenda: 'YYYY年MMM月D日'
      day: 'YYYY年MMM月D日'
    buttonText:
      today: '今日'
      month: '月'
      week: '週'
      day: '日'
    monthNames: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
    monthNamesShort: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
    dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日']
    dayNamesShort: ['日', '月', '火', '水', '木', '金', '土']
    